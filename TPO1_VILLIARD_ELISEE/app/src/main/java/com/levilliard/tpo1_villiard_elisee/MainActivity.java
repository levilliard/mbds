package com.levilliard.tpo1_villiard_elisee;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    Button btnToast;
    Button btnInc;
    TextView myText;

    int inc = 0;
    int n_click = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        btnToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),n_click + "", Toast.LENGTH_LONG).show();
            }
        });

        btnInc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inc += 10;
                n_click++;
                myText.setText(inc + "");
            }
        });

    }

    private void initViews(){
        btnToast = (Button)findViewById(R.id.btn_toast);
        btnInc = (Button)findViewById(R.id.btn_inc);
        myText = (TextView)findViewById(R.id.my_text1);
    }
}