package com.levilliard.tp02_villiard_elisee;

public class Constant {
    public static int SUM_CODE = 1;
    public static int MULTIPLY_CODE = 2;
    public static int MINUS_CODE = 3;
    public static int DIVIDE_CODE = 4;
    public static int ERROR_CODE = -1;
    public static final int COMPUTE_CODE = 1;

}
