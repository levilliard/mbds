package com.levilliard.tp02_villiard_elisee;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static com.levilliard.tp02_villiard_elisee.Constant.COMPUTE_CODE;

public class MainActivity extends AppCompatActivity {

    private EditText editTextNb1;
    private EditText editTextNb2;
    private Button buttonCompute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == COMPUTE_CODE && data != null) {
                int operation = data.getIntExtra("compute_code", -1);
                Log.w("operation", operation + "");

                if(operation == Constant.DIVIDE_CODE){
                    double result = data.getDoubleExtra("result", 0.00);
                    Toast.makeText(this, "Result is " + result, Toast.LENGTH_SHORT).show();
                }else{
                    int result = data.getIntExtra("result", -1);
                    Toast.makeText(this, "Result is " + result, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void initViews() {
        editTextNb1 = findViewById(R.id.nb1);
        editTextNb2 = findViewById(R.id.nb2);
        buttonCompute = findViewById(R.id.compute);
        buttonCompute.setEnabled(false);

        buttonCompute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Click", Toast.LENGTH_SHORT).show();

                // Get the contents of the input texts
                String textNb1 = editTextNb1.getText().toString();
                String textNb2 = editTextNb2.getText().toString();

                Intent intent = new Intent(MainActivity.this, ComputeActivity.class);
                intent.putExtra("nb1", textNb1);
                intent.putExtra("nb2", textNb2);
                //startActivity(intent);

                startActivityForResult(intent, COMPUTE_CODE);
            }
        });


        editTextNb1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0){
                    buttonCompute.setEnabled(false);
                }

                if (editTextNb2.getText().toString().trim().equalsIgnoreCase("")) {
                    editTextNb2.setError("This field can not be blank");
                }else if (!editTextNb1.getText().toString().trim().equalsIgnoreCase("")) {
                    buttonCompute.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        editTextNb2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0){
                    buttonCompute.setEnabled(false);
                }

                if (editTextNb1.getText().toString().trim().equalsIgnoreCase("")) {
                    editTextNb1.setError("This field can not be blank");
                }else if (!editTextNb2.getText().toString().trim().equalsIgnoreCase("")) {
                    buttonCompute.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
}