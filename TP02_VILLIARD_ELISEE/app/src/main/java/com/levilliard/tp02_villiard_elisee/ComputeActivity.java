package com.levilliard.tp02_villiard_elisee;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ComputeActivity extends AppCompatActivity {

    private TextView textViewNb1;
    private TextView textViewNb2;
    private Button buttonSum;
    private Button buttonMinus;
    private Button buttonMultiply;
    private Button buttonDivide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compute);

        this.setUpViews();
    }

    private void setUpViews() {

        textViewNb1 = findViewById(R.id.textNb1);
        textViewNb2 = findViewById(R.id.textNb2);
        buttonSum = findViewById(R.id.sum);
        buttonMinus = findViewById(R.id.minus);
        buttonMultiply = findViewById(R.id.multiply);
        buttonDivide = findViewById(R.id.divide);

        Intent intent = getIntent();
        String nb1 = intent.getStringExtra("nb1");
        String nb2 = intent.getStringExtra("nb2");

        textViewNb1.setText("Nombre 1 : "+ nb1);
        textViewNb2.setText("Nombre 2 : "+ nb2);

        buttonSum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = Integer.parseInt(nb1) + Integer.parseInt(nb2);
                Intent intent = new Intent();
                intent.putExtra("result", result);
                intent.putExtra("compute_code", Constant.SUM_CODE);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = Integer.parseInt(nb1) - Integer.parseInt(nb2);
                Intent intent = new Intent();
                intent.putExtra("result", result);
                intent.putExtra("compute_code", Constant.MINUS_CODE);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        buttonMultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = Integer.parseInt(nb1) * Integer.parseInt(nb2);
                Intent intent = new Intent();
                intent.putExtra("result", result);
                intent.putExtra("compute_code", Constant.MULTIPLY_CODE);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        buttonDivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();

                int divide = Integer.parseInt(nb2);
                if(divide != 0){
                    try{
                        double result = (double) Integer.parseInt(nb1) / divide;
                        Log.w("result", result+"");
                        intent.putExtra("result", result);
                        intent.putExtra("compute_code", Constant.DIVIDE_CODE);
                        setResult(RESULT_OK, intent);
                        finish();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(ComputeActivity.this, "Operation impossible", Toast.LENGTH_SHORT).show();
                    finish();
                }

            }
        });
    }
}